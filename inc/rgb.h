/*
 * rgb.h
 *
 *  Created on: 16.07.2018
 *      Author: alex
 */

#ifndef RGB_H_
#define RGB_H_

#include "stm32f10x.h"

#define RGB_MAX 255

namespace rgb {

typedef struct {
	u8 r, g, b;
} rgb;

struct hsv;

rgb hsv2rgb(hsv in);

struct hsv {
	int h, s, v;
	operator rgb() { return hsv2rgb(*this); }
};

struct expandColor {
	u8 b[8];
	expandColor() {}
	expandColor(u8 x) {
		for (int i = 0; i < 8; i++) {
			bool byte = x & (1 << i);
			if (byte) {
				b[7 - i] = 60;
			} else {
				b[7 - i] = 30;
			}
		}
	}
};

struct expandRGB {
	expandColor g, r, b;
	expandRGB() {}
	expandRGB(const ::rgb::rgb &o) {
		r = o.r;
		g = o.g;
		b = o.b;
	}
};

} // namespace rgb

#endif /* RGB_H_ */
