/*
 * clock.cpp
 *
 *  Created on: 21.12.2018
 *      Author: alex
 */

#include "clock.h"
#include "stm32f10x.h"

Clock::Clock() {
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  RCC->BDCR |= RCC_BDCR_RTCSEL_LSE;
  PWR->CR |= PWR_CR_DBP;
  if (RCC->CSR & RCC_CSR_PORRSTF) {
  }
}
