/*
 * animator_fade.cpp
 *
 *  Created on: 07.12.2018
 *      Author: alex
 */
#include "simple_animators.h"

rgb::AnimatorFade::AnimatorFade(int step, int hue) :
		step(step), hue(hue) {

}

void rgb::AnimatorFade::render(std::array<rgb, NUMLEDS> &colors) {
	static int bright = 0;
	static int mode = 0;
	::rgb::hsv color;
	color.h = hue;
	color.s = 0xFF;
	color.v = br * bright / 0xFF;

	if (mode == 0) {
		bright += step;
		if (bright >= 0xFF) {
			bright = 0xFF;
			mode = 1;
		}
	} else {
		bright -= step;
		if (bright <= 0) {
			bright = 0;
			mode = 0;
		}
	}

	const ::rgb::rgb rgbColor = color;
	std::fill(colors.begin(), colors.end(), rgbColor);
}

rgb::AnimatorFadeHue::AnimatorFadeHue(int bright, int hue) :
		brightStep(bright), hueStep(hue) {
}

void rgb::AnimatorFadeHue::render(std::array<rgb, NUMLEDS> &colors) {
	static int bright = 0, hue = 0;
	static int mode = 0;
	::rgb::hsv color;
	color.h = hue;
	color.s = 0xFF;
	color.v = br * bright / 0xFF;

	if (mode == 0) {
		bright += brightStep;
		if (bright >= 0xFF) {
			bright = 0xFF;
			mode = 1;
		}
	} else {
		bright -= brightStep;
		if (bright <= 0) {
			bright = 0;
			hue += hueStep;
			hue %= 360;
			mode = 0;
		}
	}

	const ::rgb::rgb rgbColor = color;
	std::fill(colors.begin(), colors.end(), rgbColor);
}

void rgb::AnimatorConstColor::render(std::array<rgb, NUMLEDS> &colors) {
	::rgb::hsv color;
	color.h = hue;
	color.s = 0xFF;
	color.v = br;
	const ::rgb::rgb rgbColor = color;
	std::fill(colors.begin(), colors.end(), rgbColor);
}

rgb::AnimatorConstColor::AnimatorConstColor(int hue) :
		hue(hue) {

}
