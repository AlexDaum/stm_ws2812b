#include <memory>
#include "adc_rand_seed.h"
#include "animator.h"
#include "rgbstrip.h"
#include "simple_animators.h"
#include "systick.h"

#define RGB_STEP 36
const int RGB_LED_OFFS = 0;
#define BRI_STEP 2
volatile int phase = 180;
volatile int bright = 0xFF;
void doFade();

extern "C" void TIM4_IRQHandler();


int main() {
//  adc_rand_seed();
  SystemCoreClockUpdate();
  systick_start();
  RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
  TIM4->ARR = 0x100;
  TIM4->PSC = SystemCoreClock >> 13;
  TIM4->DIER |= TIM_DIER_UIE;
  TIM4->CR1 |= TIM_CR1_CEN;
  NVIC_EnableIRQ(TIM4_IRQn);
  NVIC_SetPriorityGrouping(0b101);
  NVIC_SetPriority(TIM4_IRQn, 0xE);
  doFade();
  RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
  GPIOC->CRH &= ~(0xF << (4 * (15 - 8)));
  GPIOC->CRH |= 1 << (4 * (15 - 8));
  GPIOC->BSRR = 1 << 13;
  GPIOC->CRH |= 1 << (4 * (14 - 8));
  GPIOC->BSRR = 1 << 14;
  while (1) {
    __WFI();
  }
  return 0;
}

void doFade() {
  rgb::Animator *anim = new rgb::AnimatorVortschrittsbalken(0, 2, 2);
  rgb::strip.animate(anim);
}

void doHueWheel() {}

extern "C" void TIM4_IRQHandler() {
  rgb::strip.updateAnimation();
  rgb::strip.show();
  TIM4->SR &= ~TIM_SR_UIF;
}
