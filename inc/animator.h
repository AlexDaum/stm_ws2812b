/*
 * animator.h
 *
 *  Created on: 07.12.2018
 *      Author: alex
 */

#ifndef ANIMATOR_H_
#define ANIMATOR_H_

#include "defs.h"
#include "rgb.h"
#include <array>
namespace rgb {
class Animator {
private:

public:
	virtual void render(std::array<rgb, NUMLEDS> &colors) = 0;
	Animator();
	virtual ~Animator();
};
}

#endif /* ANIMATOR_H_ */
