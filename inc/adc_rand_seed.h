/*
 * adc_rand_seed.h
 *
 *  Created on: 14.12.2018
 *      Author: alex
 */

#ifndef ADC_RAND_SEED_H_
#define ADC_RAND_SEED_H_

#include <stdlib.h>
void adc_rand_seed();

#endif /* ADC_RAND_SEED_H_ */
