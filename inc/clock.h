/*
 * clock.h
 *
 *  Created on: 21.12.2018
 *      Author: alex
 */

#ifndef CLOCK_H_
#define CLOCK_H_

class Clock;

__attribute__((weak)) void clock_callback(Clock &clock);

class Clock {
  Clock();
  void setNextTime(int time);
};

Clock clock;

#endif /* CLOCK_H_ */
