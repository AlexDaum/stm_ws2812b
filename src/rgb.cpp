#include "defs.h"
#include <rgb.h>

rgb::rgb rgb::hsv2rgb(hsv in) {
	int hh, p, q, t, ff;
	int i;
	rgb out;

	if (in.s == 0) {
		out.r = in.v;
		out.g = in.v;
		out.b = in.v;
		return out;
	}
	hh = in.h % 360;
	//	hh /= 60;
	i = hh / 60;
	ff = hh - 60 * i;
	ff *= 255;
	ff /= 60;

	p = in.v * (0xFF - in.s);
	q = in.v * (0xFF - ((in.s * ff) / 0xFF));
	t = in.v * (0xFF - ((in.s * (0xFF - ff)) / 0xFF));

	p /= 0xFF;
	q /= 0xFF;
	t /= 0xFF;

	switch (i) {
	case 0:
		out.r = in.v;
		out.g = t;
		out.b = p;
		break;
	case 1:
		out.r = q;
		out.g = in.v;
		out.b = p;
		break;
	case 2:
		out.r = p;
		out.g = in.v;
		out.b = t;
		break;

	case 3:
		out.r = p;
		out.g = q;
		out.b = in.v;
		break;
	case 4:
		out.r = t;
		out.g = p;
		out.b = in.v;
		break;
	case 5:
	default:
		out.r = in.v;
		out.g = p;
		out.b = q;
		break;
	}
	return out;
}
