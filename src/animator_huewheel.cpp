#include "simple_animators.h"

rgb::AnimatorHueWheel::AnimatorHueWheel(int hueStep, int hueIncrement,
										int startHue)
	: hueStep(hueStep), hueIncrement(hueIncrement), hue(startHue) {}

void rgb::AnimatorHueWheel::render(std::array<rgb, NUMLEDS> &colors) {
	for (int i = 0; i < NUMLEDS; i++) {
		::rgb::hsv color;
		color.h = hue + hueStep * i;
		color.s = 0xFF;
		color.v = br;

		colors[i] = color;
	}
	hue += hueIncrement;
}
