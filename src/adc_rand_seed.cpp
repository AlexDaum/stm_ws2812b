/*
 * adc_rand_seed.cpp
 *
 *  Created on: 14.12.2018
 *      Author: alex
 */

#include "adc_rand_seed.h"
#include "stm32f10x.h"
void adc_rand_seed() {
  RCC->CFGR |= RCC_CFGR_ADCPRE_1;
  RCC->APB2ENR |= RCC_APB2ENR_ADC1EN | RCC_APB2ENR_IOPAEN;
  ADC1->CR2 |= ADC_CR2_ADON;
  ADC1->CR2 |= ADC_CR2_EXTSEL;
  GPIOA->CRL &= 0xF;
  u32 seed = 0;

  for (int i = 0; i < 8; i++) {
    ADC1->CR2 |= ADC_CR2_SWSTART;
    while (!(ADC1->SR & ADC_SR_EOC))
      ;
    seed |= ADC1->DR & 0xF;
    seed <<= 4;
  }

  ADC1->CR2 &= ~ADC_CR2_ADON;
  RCC->AHBENR |= RCC_APB2ENR_ADC1EN;
  srand(seed);
}
