/*
 * systick.h
 *
 *  Created on: 03.05.2018
 *      Author: alex
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

void systick_start();
void systick_stop();
uint32_t systick_getCount();
void systick_delay(uint32_t millis);
void systick_delay_until(uint32_t millis);
#ifdef __cplusplus
}

class sTick {
public:
	static void start() {
		systick_start();
	}

	static void stop() {
		systick_stop();
	}
	static uint32_t getCount() {
		return systick_getCount();
	}
	static void delay(uint32_t millis) {
		systick_delay(millis);
	}
	static void delay_until(uint32_t millis) {
		systick_delay_until (millis);
	}
};
#endif
#endif /* SYSTICK_H_ */
