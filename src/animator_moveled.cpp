/*
 * animator_moveled.cpp
 *
 *  Created on: 13.12.2018
 *      Author: alex
 */

#include "simple_animators.h"

#define A (65531)
#define P 11854

volatile static int count = 0;

rgb::AnimatorVortschrittsbalken::AnimatorVortschrittsbalken(int hue,
                                                            int hueStep,
                                                            int hueIncrement)
    : hue(hue), hueStep(hueStep), hueIncrement(hueIncrement) {
  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  TIM2->PSC = P;
  TIM2->ARR = A;
  TIM2->DIER |= TIM_DIER_UIE;
  TIM2->EGR = TIM_EGR_UG;
  TIM2->SR = ~TIM_SR_UIF;
  NVIC_EnableIRQ(TIM2_IRQn);
  TIM2->CR1 |= TIM_CR1_CEN;
}

void rgb::AnimatorVortschrittsbalken::render(std::array<rgb, NUMLEDS> &colors) {
  int cnt = count;
  if (cnt >= NUMLEDS) {
    rgb green = {0, br, 0};
    std::fill(colors.begin(), colors.end(), green);
    return;
  }
  u32 part = TIM2->CNT;
  int not_set = NUMLEDS - cnt;
  int move = (part * not_set) / A;
  int i = 0;
  hsv set;
  set.v = br;
  set.s = 255;
  for (; i < move; i++) {
    ::rgb::hsv color;
    color.h = hue + hueStep * i;
    color.s = 0xFF;
    color.v = br / 4;
    colors[i] = color;
  }
  // i == move
  set.h = hue + hueStep * (NUMLEDS - i);
  int step = A / not_set;
  int partial = part % step;
  partial = (partial * 0x100) / step;
  set.v = br * (0x100 + partial) / 0x400;
  colors[i++] = set;
  colors[i++] = set;
  for (; i < not_set; i++) {
    ::rgb::hsv color;
    color.h = hue + hueStep * i;
    color.s = 0xFF;
    color.v = br / 4;
    colors[i] = color;
  }
  for (; i < NUMLEDS; i++) {
    ::rgb::hsv color;
    color.h = hue + hueStep * (NUMLEDS - i);
    color.s = 0xFF;
    color.v = br;
    colors[i] = color;
  }
  hue += hueIncrement;
}

rgb::AnimatorVortschrittsbalken::~AnimatorVortschrittsbalken() {
  RCC->APB1ENR &= ~RCC_APB1ENR_TIM2EN;
}

extern "C" void TIM2_IRQHandler() {
  if (TIM2->SR & TIM_SR_UIF) {
    count++;
    TIM2->SR &= ~TIM_SR_UIF;
  }
}
