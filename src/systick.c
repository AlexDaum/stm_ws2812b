#include "systick.h"
#include "stm32f10x.h"
volatile uint32_t tick = 0;
extern uint32_t SystemCoreClock;

void systick_start() {
	SysTick_Config(SystemCoreClock / 1000);
}
void systick_stop() {

}
uint32_t systick_getCount() {
	return tick;
}
void systick_delay(uint32_t millis) {
	tick = 0;
	while (tick < millis)
		;
}

void __attribute__((weak)) SysTickExtraWork() {

}

void SysTick_Handler() {
	tick++;
	SysTickExtraWork();
}

void systick_delay_until(uint32_t millis) {
	while (tick < millis) {

	}
}
