#include "rgbstrip.h"
#include <algorithm>

rgb::rgbstrip rgb::strip;
std::size_t &stripBuf = rgb::strip.bufInUse;
extern "C" void DMA1_Channel2_IRQHandler() {
	static u32 cnt = 0;
	static u32 addr = 0;
	static int cont = true;
	if (DMA1->ISR & DMA_ISR_TCIF2) {
		DMA1_Channel2->CCR &= ~DMA_CCR2_EN;
		DMA1_Channel2->CNDTR = cnt;
		DMA1_Channel2->CMAR = addr;
		DMA1->IFCR = DMA_IFCR_CTCIF2;
		if (cont)
			DMA1_Channel2->CCR |= DMA_CCR2_EN;
		else
			TIM3->CCR3 = 0;
	}
	if (DMA1->ISR & DMA_ISR_HTIF2) {
		cont = true;
		stripBuf = stripBuf ? 0 : 1;
		cnt = 0;
		addr = (u32)(&rgb::strip.buffer[stripBuf]);
		if (rgb::strip.index < NUMLEDS) {
			cnt = rgb::strip.populate_buffer();
			cnt *= sizeof(rgb::expandRGB);
		} else {
			cont = false;
		}
		DMA1->IFCR = DMA_IFCR_CHTIF2;
	}
}

std::size_t rgb::rgbstrip::populate_buffer() {
	GPIOC->BRR = 1 << 15;
	std::size_t i = 0;
	for (; i < buffer[bufInUse].size() && index < NUMLEDS; i++) {
		buffer[bufInUse][i] = colors[index];
		index++;
	}
	GPIOC->BSRR = 1 << 15;
	return i;
}

void rgb::rgbstrip::show() {
	index = 0;
	DMA1_Channel2->CCR &= ~DMA_CCR2_EN;
	DMA1_Channel2->CNDTR = sizeof(buffer[bufInUse]);
	DMA1_Channel2->CPAR = (u32)&TIM3->CCR3;
	populate_buffer();
	DMA1_Channel2->CMAR = (u32)(&buffer[bufInUse]);
	TIM3->SR = 0;
	TIM3->CCR3 = 0;
	TIM3->CNT = 0;
	DMA1_Channel2->CCR |= DMA_CCR2_EN;
}

rgb::rgbstrip::rgbstrip() {
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;

	TIM3->ARR = 90;
	TIM3->PSC = 0;
	TIM3->CCR3 = 0;
	TIM3->CCER |= TIM_CCER_CC3E;
	TIM3->CCMR2 |=
		TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3FE | TIM_CCMR2_OC3PE;
	TIM3->DIER |= TIM_DIER_CC3DE;
	TIM3->DCR &= ~0xF;
	TIM3->DCR |= 14; // DMA Target to CCR3
	TIM3->EGR = TIM_EGR_UG;
	TIM3->CR1 |= TIM_CR1_CEN;
	// Transfer with MSIZE=8 PSIZE=16 with High Priority
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	DMA1_Channel2->CCR |= DMA_CCR2_PL_1 | DMA_CCR2_MINC | DMA_CCR2_DIR |
						  DMA_CCR2_PSIZE_0 | DMA_CCR2_TCIE | DMA_CCR2_HTIE;
	DMA1_Channel2->CNDTR = sizeof(buffer[0]);
	DMA1_Channel2->CPAR = (u32)&TIM3->CCR3;
	DMA1_Channel2->CMAR = (u32)&buffer[0];

	NVIC_EnableIRQ(DMA1_Channel2_IRQn);

	GPIOB->CRL &= ~0x0000000F;
	GPIOB->CRL |= 0x00000009;
}

void rgb::rgbstrip::updateAnimation() {
	if (animator != nullptr) {
		animator->render(this->colors);
	}
}

void rgb::rgbstrip::animate(Animator *anim) {
	if (this->animator != nullptr) {
		delete this->animator;
	}
	this->animator = anim;
}
