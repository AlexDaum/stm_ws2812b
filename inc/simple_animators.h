/*
 * simple_animators.h
 *
 *  Created on: 07.12.2018
 *      Author: alex
 */

#ifndef SIMPLE_ANIMATORS_H_
#define SIMPLE_ANIMATORS_H_
#include "animator.h"
#include "defs.h"
namespace rgb {
class AnimatorFade: public Animator {
	const int step, hue;
public:
	AnimatorFade(int step, int hue);
	virtual void render(std::array<rgb, NUMLEDS> &colors);
};
class AnimatorFadeHue: public Animator {
	const int brightStep, hueStep;
public:
	AnimatorFadeHue(int brightnessStep, int hueStep);
	virtual void render(std::array<rgb, NUMLEDS> &colors);
};

class AnimatorHueWheel: public Animator {
	const int hueStep, hueIncrement;
	int hue;
public:
	AnimatorHueWheel(int hueStep, int hueIncrement, int startHue = 0);
	virtual void render(std::array<rgb, NUMLEDS> &colors);
};

class AnimatorConstColor: public Animator {
	const int hue;
public:
	AnimatorConstColor(int hue);
	virtual void render(std::array<rgb, NUMLEDS> &colors);
};

class AnimatorVortschrittsbalken: public Animator {
	int hue;
	const int hueStep, hueIncrement;
public:
	AnimatorVortschrittsbalken(int hue, int hueStep, int hueIncrement);
	virtual ~AnimatorVortschrittsbalken();
	virtual void render(std::array<rgb, NUMLEDS> &colors);
};
}
#endif /* SIMPLE_ANIMATORS_H_ */
