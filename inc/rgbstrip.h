/*
 * rgbstrip.h
 *
 *  Created on: 18.07.2018
 *      Author: alex
 */

#ifndef RGBSTRIP_H_
#define RGBSTRIP_H_

#include <defs.h>
#include <rgb.h>
#include <stm32f10x.h>
#include <array>
#include <cstddef>
#include <memory>
#include "animator.h"

extern "C" void DMA1_Channel2_IRQHandler();
int main();
namespace rgb {

class rgbstrip {
private:
	friend void ::DMA1_Channel2_IRQHandler();
	friend int ::main();
	std::array<expandRGB, BUFSIZE> buffer[2];

	std::array<rgb, NUMLEDS> colors;
	std::size_t index = 0;
	Animator *animator = nullptr;
	std::size_t populate_buffer();
public:
	std::size_t bufInUse = 0;
	friend class Animator;
	rgbstrip();
	void setColor();
	void animate(Animator *animator);
	void updateAnimation();
	void show();
};

extern rgbstrip strip;

}

#endif /* RGBSTRIP_H_ */
